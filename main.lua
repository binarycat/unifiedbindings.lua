-- demo: render an NES controller

local bindings = require("unifiedbindings")

-- we use integers for ease of implementation, but strings can also be used (useful if you have a lot of different actions)
-- in fact you could even use sentinel table values and check pointer equality, though that is not reccomended.
local nes_bindings = bindings.new({
	key = {
		up = 1,
		down = 2,
		left = 3,
		right = 4,
	},
	-- scancodes are layout-independant
	scancode = {
		z = "A",
		x = "B",
		esc = "START",
		tab = "SELECT",
	}
})

local HULL = { 0.5, 0.5, 0.5 }

local DOWN = { 0, 1, 0 }

local UP = { 1, 0, 0 }

local LAYOUT_HULL = { 10, 10, 150, 50 }

local LAYOUT = {
	{ 30, 20, 10, 10 },
	{ 30, 40, 10, 10 },
	{ 10, 30, 10, 10 },
	{ 50, 30, 10, 10 },
}

local color = {}



function love.draw()
	for k,v in pairs(LAYOUT) do
		love.graphics.setColor(color[k] or UP)
		love.graphics.rectangle("fill", unpack(v))
	end
end

nes_bindings:install(function(cur, prev)
	for bind_id, state in ipairs(cur) do
		if state then
			color[bind_id] = DOWN
		else
			color[bind_id] = UP
		end
	end
end)
