-- unified event-based keybindings

-- binding kinds:
-- * key
-- * scancode
-- * button (gamepad buttons)
-- * mouse (TODO) (this one specifically refers to mouse buttons)
-- TODO: add more

-- binding types:
-- * bool
-- * scalar (TODO)

local bindings = {}

bindings.__index = bindings

function bindings.new(tbl)
	return setmetatable(tbl, bindings)
end

function bindings:install(func, lv)
	-- previous and current button state
	local prev = {}
	local cur = {}
	
	local lv = lv or love
	
	-- set bool/button type binding
	local function set_b_generic(kind, x, value)
		if self[kind] then
			local id = self[kind][x]
			if id then
				cur[id] = value
			end
		end
	end
	
	local function set_b(kind, x)
		set_b_generic(kind, x, true)
	end
	
	local function unset_b(kind, x)
		set_b_generic(kind, x, false)
	end
	
	function lv.keypressed(key, scancode, isrepeat)
		if not isrepeat then
			set_b("key", key)
			set_b("scancode", scancode)
		end
	end
	
	function lv.keyreleased(key, scancode)
		if not isrepeat then
			unset_b("key", key)
			unset_b("scancode", scancode)
		end
	end
	
	function lv.gamepadpressed(_, button)
		set_b("button", b)
	end
		
	function lv.gamepadreleased(_, button)
		unset_b("button", b)
	end
	
	function lv.update(dt)
		func(cur, prev, dt)
		prev = cur
		cur = {}
		for k,v in pairs(prev) do
			cur[k] = v
		end
	end
end

return bindings
